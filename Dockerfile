FROM tomcat:9-jdk11
RUN apt-get update \
  && apt-get install -y cron wget unzip

ENV RELEASE https://github.com/voyanttools/VoyantServer/releases/download/2.6.17/VoyantServer.zip
WORKDIR /usr/local/tomcat
RUN wget $RELEASE
RUN unzip *.zip \
  && rm *.zip \
  && mv VoyantServer*/_app webapps/ROOT \
  && mkdir -p /default-data \
  && mv VoyantServer*/data/* /default-data \
  && rm VoyantServer* -r
COPY --chmod=0755 *.sh /
ENTRYPOINT ["/entrypoint.sh"]

# app should be available on host:8080

LABEL maintainer="zacanbot@gmail.com"
LABEL name="lincsproject/voyant"
LABEL org.opencontainers.image.source https://gitlab.com/calincs/access/voyant-docker
LABEL org.opencontainers.image.title "Voyant Tools"
LABEL org.opencontainers.image.description "Voyant Tools is a web-based reading and analysis environment for digital texts."
LABEL org.opencontainers.image.version "3"
LABEL org.opencontainers.image.licenses "GPL-3.0"
LABEL org.opencontainers.image.authors "Voyant by https://github.com/voyanttools; this image by zacanbot"
