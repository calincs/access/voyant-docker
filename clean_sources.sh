#!/bin/bash

echo "Finding old document sources ..."

DATA_DIR="/voyant-data"
TROMBONE_DIR="${DATA_DIR}/trombone5_2"
SOURCE_DIR="${TROMBONE_DIR}/stored_document_sources"
CUTOFF_DATE=$(date -d "180 days ago" +%s)
OUTPUT_FILE="ids.txt"
# Clear the output file if it already exists
> $OUTPUT_FILE

# Function to process each directory
process_dir() {
  local dir=$1
  local last_access_time=0

  if [ -f "$dir/metadata.xml" ]; then
    last_access_time=$(stat -c %X "$dir/metadata.xml")
    if [[ $last_access_time -le $CUTOFF_DATE ]]; then
      doc_id=$(basename "$dir")
      echo "$doc_id"
    fi
  else
    # tag source without metadata for removal anyway
    doc_id=$(basename "$dir")
    echo "$doc_id"
  fi
}

export -f process_dir
export CUTOFF_DATE

# Use find to list directories and xargs to process them in parallel
find "$SOURCE_DIR" -mindepth 1 -maxdepth 1 -type d -print0 | xargs -0 -n 1 -P 12 bash -c 'process_dir "$@"' _ > "$OUTPUT_FILE"

echo "Processing complete. Source IDs have been saved to ${OUTPUT_FILE}."

# Show the number of document IDs in the output
echo "Found $(wc -l < "$OUTPUT_FILE") document IDs."

# Delete directories represented by the IDs in the file
while IFS= read -r doc_id; do
  rm -rf "${SOURCE_DIR}/${doc_id}"
done < "$OUTPUT_FILE"

echo "Processing complete. Old documents have been deleted."



# fast but inaccurate (based on create dates)

# DAYS_AGO=180

# # Declare an array to hold source IDs to be deleted
# source_ids=()

# # Find directories not modified in the last DAYS_AGO days
# find "$SOURCE_DIR" -mindepth 1 -maxdepth 1 -type d -mtime +$DAYS_AGO -print0 | while IFS= read -r -d '' dir; do
#   doc_id=$(basename "$dir")
#   source_ids+=("$dir")
# done

# num_to_delete=${#source_ids[@]}
# echo "Number of source documents older than $DAYS_AGO days being deleted: $num_to_delete ..."

# for dir in "${source_ids[@]}"; do
#   rm -rf "$dir"
# done

# echo "Deleted $num_to_delete source directories."
