#!/bin/bash

echo "Deleting tmp uploads and cache ..."

DATA_DIR="/voyant-data"
TROMBONE_DIR="${DATA_DIR}/trombone5_2"

rm "${DATA_DIR}/*.tmp"
rm -rf "${DATA_DIR}/tmp.voyant.uploads"
mkdir "${DATA_DIR}/tmp.voyant.uploads"

# delete cache files older than 10 days
find "${TROMBONE_DIR}/cache" -type f -mtime +10 -exec rm -f {} \;
