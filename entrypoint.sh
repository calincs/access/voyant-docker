#!/bin/bash

BASE=/voyant-data

# move data files from the image folder to the mounted data folder
cp -nr /default-data/* $BASE

# create cron job to execute cleanup scripts daily
crontab -l | { cat; echo "0 0 * * * /clean_cache.sh"; } | crontab -
crontab -l | { cat; echo "0 1 * * * /clean_corpora.sh"; } | crontab -
crontab -l | { cat; echo "0 2 * * * /clean_sources.sh"; } | crontab -

service cron start

# increase maximum post size
sed -i "s%connectionTimeout=\"20000\"%maxPostSize=\"-1\" connectionTimeout=\"120000\"%g" $CATALINA_HOME/conf/server.xml
# increase cache
sed -i "s%<Context>%<Context>\n    <Resources cacheMaxSize=\"51200\" />%g" $CATALINA_HOME/conf/context.xml

# continue execution
# exec "$@"
catalina.sh run