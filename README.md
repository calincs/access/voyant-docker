# Voyant Docker

Voyant is hosted and available here: [https://voyant.lincsproject.ca/](https://voyant.lincsproject.ca/)

This project exists to containerise VoyantServer and to enable deployment on the LINCS infrastructure. _Voyant_ is mostly the frontend, and _Trombone_ is the backend.

- Voyant source: [https://github.com/voyanttools/Voyant](https://github.com/voyanttools/Voyant)
- Trombone source: [https://github.com/voyanttools/trombone](https://github.com/voyanttools/trombone)
- Standalone server: [https://github.com/voyanttools/VoyantServer](https://github.com/voyanttools/VoyantServer)

## Running Voyant

Clone the repository, or just download the [Docker Compose file](./docker-compose.yaml), and run:

```bash
docker compose up
```

Voyant will be available here: [http://localhost:8080](http://localhost:8080) and Spyral Notebooks here: [http://localhost:8080/spyral](http://localhost:8080/spyral)

To stop:

```bash
docker compose down
```

To stop and delete the Voyant image (will need to download again to start Voyant):

```bash
docker compose down --rmi all
```

## Developers

To build your own Voyant Docker image:

Comment the `image: ***` line in the docker-compose.yaml file and un-comment the build context lines. Then run:

```bash
docker compose up --build
```

If you want to build a Docker image from the latest sources (and not the voyant-server release), then use the `Dockerfile-rebuild` file instead of the default `Dockerfile` in this repo.
